---
title: "模型理论和简单实现"
author: "WHH"
date: "2020年3月"
institute: 中南财经政法大学统计与数学学院
csl: ./style/chinese-gb7714-2005-numeric.csl
css: ./style/markdown.css
bibliography: [./Bibfile.bib]
#nocite: '@*'
eqnPrefixTemplate: ($$i$$)
link-citations: true
linkReferences: true
chapters: true
tableEqns: false
autoEqnLabels: false
classoption: "aspectratio=1610"
---

在有关金融市场波动率的研究中，若想要探究经济因素对金融市场的影响，则传统的GARCH
类模型略显不足，它只能处理同频数据。Ghysels等引入了混频抽样.

$$\begin{aligned}S_{t}^{e}&=\sum_{i=1}^{t}\left(\Delta NAV_{i}-\overline {\Delta
NAV_i}\right)^2\\
&=\sum_{i=1}^{n}\left(\Delta NAV_{t}-\left(a^{0}+a^1 \Delta
Index^1+a^{2} \Delta Index^{2}+a^3\Delta Index^{3}+\cdots+a^m \Delta
Index^{m}\right)\right)^2\end{aligned}$$

$$\begin{aligned}
&\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^2+\hat{a}^{2}
Index_{i}^{2}+\hat a^{3} Index_{3}^{2}+\cdots+\hat a^{m}
Index_{i}^{m}\right)=\sum_{i=1}^t NAV_{i}\\
&\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^{1}+\hat{a}^{2}
Index_{i}^{2}+\hat{a}^{3} Index_{i}^{3}+\cdots+\hat{a}^{m} Index_{i}^{m}\right)
Index_{i}^{1}=\sum_{i=1}^t NAV_{i}Index_{i}^{1}\\
&\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^{1}+\hat{a}^{2}
Index_{i}^{2}+\hat{a}^{3} Index_{i}^{3}+\cdots+\hat{a}^{m} Index_{i}^{m}\right)
Index_{i}^{2}=\sum_{i=1}^t NAV_{i}Index_{i}^{2}\\
&\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^{1}+\hat{a}^{2}
Index_{i}^{2}+\hat{a}^{3} Index_{i}^{3}+\cdots+\hat{a}^{m} Index_{i}^{m}\right)
Index_{i}^{3}=\sum_{i=1}^t NAV_{i}Index_{i}^{3}\\ \vdots\\
&\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^{1}+\hat{a}^{2}
Index_{i}^{2}+\hat{a}^{3} Index_{i}^{3}+\cdots+\hat{a}^{m} Index_{i}^{m}\right)
Index_{i}^{m}=\sum_{i=1}^t NAV_{i}Index_{i}^{m}
\end{aligned}$$

$$\left(\begin{array}{cccc}
n & \sum Index_{i}^1 & \cdots & \sum Index_{i}^m \\
\sum Index_{i}^1 & \sum (Index_{i}^{1})^2 & \cdots & \sum Index_{i}^1 Index_{i}^m \\
\cdots & \cdots & \cdots & \cdots \\
\sum Index_{i}^m & \sum Index_{i}^m Index_{i}^1 & \cdots & \sum (Index_{i}^m)^{2}
\end{array}\right)\left(\begin{array}{c}
\hat{a}_{0} \\
\hat{a}_{1} \\
\cdots \\
\hat{a}_{k}
\end{array}\right)=\left(\begin{array}{cccc}
1 & 1 & \cdots & 1 \\
Index_{1}^1 & Index_{2}^1 & \cdots & Index_{t}^1 \\
\cdots & \cdots & \cdots & \cdots \\
Index_{1}^m & Index_{2}^m & \cdots & Index_{t}^m
\end{array}\right)\left(\begin{array}{c}
NAV_{1} \\
NAV_{2} \\
\cdots \\
NAV_{t}
\end{array}\right)$$

$$\left(\mathbf{Index}^{\prime} \mathbf{Index}\right)
\hat{a}=\mathbf{Index}^{\prime} \mathbf{NAV}$$

$$\left\{\begin{array}{ll}
\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^2+\hat{a}^{2}
Index_{i}^{2}+\hat a^{3} Index_{3}^{2}+\cdots+\hat a^{m}
Index_{i}^{m}\right)=\sum_{i=1}^t NAV_{i}\\
\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^{1}+\hat{a}^{2}
Index_{i}^{2}+\hat{a}^{3} Index_{i}^{3}+\cdots+\hat{a}^{m} Index_{i}^{m}\right)
Index_{i}^{1}=\sum_{i=1}^t NAV_{i}Index_{i}^{1}\\
\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^{1}+\hat{a}^{2}
Index_{i}^{2}+\hat{a}^{3} Index_{i}^{3}+\cdots+\hat{a}^{m} Index_{i}^{m}\right)
Index_{i}^{2}=\sum_{i=1}^t NAV_{i}Index_{i}^{2}\\
\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^{1}+\hat{a}^{2}
Index_{i}^{2}+\hat{a}^{3} Index_{i}^{3}+\cdots+\hat{a}^{m} Index_{i}^{m}\right)
Index_{i}^{3}=\sum_{i=1}^t NAV_{i}Index_{i}^{3}\\ \vdots\\
\sum_{i=1}^t\left(\hat{a}^{0}+\hat{a}^{1} Index_{i}^{1}+\hat{a}^{2}
Index_{i}^{2}+\hat{a}^{3} Index_{i}^{3}+\cdots+\hat{a}^{m} Index_{i}^{m}\right)
Index_{i}^{m}=\sum_{i=1}^t NAV_{i}Index_{i}^{m}
\end{array}\right.$$

```{python}

```