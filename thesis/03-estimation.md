---
title: "第 3 章"
author: "Jin"
date: '2019-04-15'
institute: 中南财经政法大学统计与数学学院
csl: ./style/chinese-gb7714-2015-numeric.csl
css: ./style/markdown.css
bibliography: [./Bibfile.bib]
notice: '@*'
eqnPrefixTemplate: ($$i$$)
link-citations: true
linkReferences: true
chapters: true
tableEqns: false
autoEqnLabels: false
classoption: "aspectratio=1610"
---





- 股票数据范围怎么选
- 整体的数据范围怎么选
- 美国的数据以美元计算，中国的以人民币计算，怎么处理

# 经济政策不确定性对中美股票市场波动率影响的研究


## 数据选取与描述性统计分析

### EPU指数的选取和描述统计分析


1. 中国EPU指数的选取和描述性统计分析

目前，中国的经济政策不确定性指数有三种，最早的一种来自Baker等人根据香港《南华早
报》的信息编制的EPU指数，之后，steven等人基于国内两大报社《人民日报》和《光明日
报》编制中国月度EPU指数和贸易政策不确定性指数。以上两种数据均源于
http://www.policyuncertainty.com/china_epu.html。本文分别用BAKER_CHINA_EPU和
STEVEN_CHINA_EPU来表示。来自香港大学的陆尚勤和黄昀基于之前编制的指数数据源太过单
一的问题，通过慧科(Wise news)提供的电子报纸资讯库，选取了中国内地较为权威的10份
报纸，分别为北京青年报、广州日报、解放日报、人民日报(海外版)、新闻晨报、南方都市
报、新京报、今晚报、文汇报和羊城晚报。在报纸文章中搜索与经济政策不确定性相关的关
键词以编制指数，数据可在网站获得
https://economicpolicyuncertaintyinchina.weebly.com ,本文用Huang_CHINA_EPU来表示
这种指数。以上几种指数的编制方法是相同的，只是数据源略有区别，接下来，本文将通过
比较分析选择最合适的一种作为经济政策不确定性指数的测度。首先给出三种指数的趋势
图，如图所示。

<div class="figure" style="text-align: center">
<img src="03-estimation_files/figure-html/EPU -1.png" alt="基于不同报纸的中国EPU指数趋势图" width="60%" />
<p class="caption">(\#fig:EPU )基于不同报纸的中国EPU指数趋势图</p>
</div>

显然，上图中BAKER_CHINA_EPU的波动性最大，STEVEN_CHINA_EPU次之，Huang_CHINA_EPU的
波动性最小，但值得注意的是，三种指数的趋势相似，变化方向大致相同，只是波动程度有
所差异，而且这种差异在2016年以后变得十分明显。通过表\@ref(tab:tab1)可以清晰的看
到，三种指数除了数据来源不同以外，在术语的设置上也有所差异，显然STEVEN_CHINA_EPU
和Huang_CHINA_EPU的选词更为合理。另外，三种指数在进行归一化处理时所选样本区间不
同，BAKER_CHINA_EPU选择将1995年1月至2011年12月的数据标准化为均值为100的序列，然
后在此基础上编制之后的数据；STEVEN_CHINA_EPU则选择了2000年到2018年的数据，
Huang_CHINA_EPU选择了2000年到2011年的数据使其均值为100。(经济政策不确定性对我国
股市的影响--艾龙)为了
更好的比较这三种指数，分别计算其相关系数，得到Huang_CHINA_EPU与STEVEN_CHINA_EPU
的相关系数为0.4746123,相关性检验得到的t值为
8.4915303,对应p值远小于0.05的显著性水
平，表示二者之间有显著的相关性;Huang_CHINA_EPU与BAKER_CHINA_EPU也有显著的相关
性，相关系数为0.4345778;STEVEN_CHINA_EPU与
BAKER_CHINA_EPU的相关系数最高，为0.8537335,并且通过
了5%的显著性检验。

综合以上可知，三种指数间具备显著的相关性,同时Huang_CHINA_EPU指数的数据来源更广，
其所选的十份报纸分布在中国经济发达地区，北京，上海，广州，天津，具有良好的代表
性，并且参考Huang等(2020)对Huang_CHINA_EPU所做的稳定性检验，排除了中国的媒体控制
这一因素对Huang_CHINA_EPU的影响，所以本文将选取基于中国大陆10份报纸的EPU指数
Huang_CHINA_EPU作为中国经济政策不确定性指标的代理变量。

2. 美国EPU指数的选取和描述性统计分析

关于美国EPU指数，Baker等人编制了两种，一种是基于10家美国大型报社的新闻报道构造的
美国EPU指数，一种是同时考量新闻信息、税收变动和经济预测差值指数三方面信息，并分
别赋予1/2、1/6、1/3的权重构造的综合指数。本文分别将其计为News_Based_EPU和
Three_Component_EPU,数据均来自http://www.policyuncertainty.com/us_monthly.html。

<div class="figure" style="text-align: center">
<img src="03-estimation_files/figure-html/AEPU-1.png" alt="不同美国EPU指数趋势图" width="60%" />
<p class="caption">(\#fig:AEPU)不同美国EPU指数趋势图</p>
</div>

从上图可以看出，美国基于新闻信息的EPU与综合考虑三种因素的EPU几乎有完全一致的变动
周期。并且他们的相关系数为0.9245304,对其进
行相关性检验，得到对应统计量t值为'r
cor.test(epu_news,aepu_three,method='pearson')$statistic。比较来看，
News_Based_EPU由于数据来源不如Three_Component_EPU广泛具有更大的波动性，并且二者
在2010年附近出现了完全相反的趋势变化。综合考虑美国实际情况，本文将选取
Three_Component_EPU作为美国经济政策不确定性的代理指标。


<table class="table table" style="margin-left: auto; margin-right: auto; margin-left: auto; margin-right: auto;">
<caption>(\#tab:tab1)中国经济政策不确定性指
数描述统计</caption>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:left;"> BAKER_CHINA_EPU </th>
   <th style="text-align:left;"> STEVEN_CHINA_EPU </th>
   <th style="text-align:left;"> Huang_CHINA_EPU </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;width: 2cm; "> 经济术语 </td>
   <td style="text-align:left;width: 2cm; "> economy/economic </td>
   <td style="text-align:left;width: 2cm; "> 经济/商业 </td>
   <td style="text-align:left;width: 2cm; "> 经济/金融 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 2cm; "> 不确定术语 </td>
   <td style="text-align:left;width: 2cm; "> uncertain/uncertainty </td>
   <td style="text-align:left;width: 2cm; "> 不确定/不明确/不明朗/未明/难料/难以预计/难以估计/难以预测/未知 </td>
   <td style="text-align:left;width: 2cm; "> 不确定/不明确/波动/震荡/动荡/不稳/未明/不明朗/不清晰/未清晰 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 2cm; "> 政策术语 </td>
   <td style="text-align:left;width: 2cm; "> policy/spending/budget/political/
interest rates/reform and government/
Beijing/authorities/tax/regulation/
regulatory/central bank/
People's Bank of China/
PBOC/deficit/WTO </td>
   <td style="text-align:left;width: 2cm; "> 财政/货币/证监会/银监会/财政部/人民银行/国家发改委/开放/改革/商务部/法律/法规/税收/国债/政府债务/央行/外经贸部/关税/政府赤字 </td>
   <td style="text-align:left;width: 2cm; "> 政策/制度/体制/战略/措施/规章/规例/条例政治/执政/政府/政委/国务院/人大/人民代表大会/中央/国家主席/总书记/国家领导人/总理/改革/整改/整治/规管/监管/财政/税/人民银行/央行/赤字/利率 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 2cm; "> 样本量 </td>
   <td style="text-align:left;width: 2cm; "> 250 </td>
   <td style="text-align:left;width: 2cm; "> 250 </td>
   <td style="text-align:left;width: 2cm; "> 250 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 2cm; "> 均值 </td>
   <td style="text-align:left;width: 2cm; "> 219.641265148163 </td>
   <td style="text-align:left;width: 2cm; "> 122.976029557161 </td>
   <td style="text-align:left;width: 2cm; "> 117.1809317402 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 2cm; "> 标准差 </td>
   <td style="text-align:left;width: 2cm; "> 221.313942489389 </td>
   <td style="text-align:left;width: 2cm; "> 102.275812304818 </td>
   <td style="text-align:left;width: 2cm; "> 39.5619609895421 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 2cm; "> 中位数 </td>
   <td style="text-align:left;width: 2cm; "> 131.504020690918 </td>
   <td style="text-align:left;width: 2cm; "> 96.25431278 </td>
   <td style="text-align:left;width: 2cm; "> 122.297081185047 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 2cm; "> 最小值 </td>
   <td style="text-align:left;width: 2cm; "> 9.0667085647583 </td>
   <td style="text-align:left;width: 2cm; "> 10.11133275 </td>
   <td style="text-align:left;width: 2cm; "> 38.1961578675666 </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 2cm; "> 最大值 </td>
   <td style="text-align:left;width: 2cm; "> 970.829895019531 </td>
   <td style="text-align:left;width: 2cm; "> 649.072535816154 </td>
   <td style="text-align:left;width: 2cm; "> 238.317174112054 </td>
  </tr>
</tbody>
</table>


具体参考《美国经济政策变动的全球效应分析：基于中美贸易摩擦视角》--司颖华

### 股票市场数据选取和描述性分析

(贸易摩擦背景下中美股市联动性分析
将美国t-1日的数据与中国t-1的数据对齐，对于由于节假日造成不同的用平滑插补，因为数
据较少，所以没有采用剔除不重复数据的办法。)

#### 股票数据选择及处理

中国股市虽有著名的上证指数，但是其编制范围仅包括在上海证券交易所上市的股票，不能
反映深圳股市，因此选用沪深300指数，该指数样本选自沪深两个证券市场，覆盖了大部分
流通市值。成份股为市场中市场代表性好，流动性高，交易活跃的优质股票，而且样本股的
行业分布状况基本与市场的行业分布状况接近因此，可以成为反映中国股市整体走势的“晴
雨表”。对于美国股市，本文选取的是标准普尔500指数(Standard and Poor’s Composite
Index)，该指数包括了在纽约证券交易所、纳斯达克等美国主要交易市场上市的500支股
票，相比道琼斯指数，具有代表性强，覆盖范围广等特点。因此，更能真实地反映美国股票
市价变动的实际情况。数据均来自https://finance.yahoo.com/。


由于本文将以金融危机和贸易战为时间节点分阶段研究经济政策不确定性对股市的影响，故
选取数据为2007年1月到2020年9月的日度数据，由于中美两国股市受自然时差和节假日的
影响，其开盘时间不同步，不能直接用于实证研究，故首先将中国股市的交易日(T)与美国
股市前一交易日(T-1)相对齐，美国股市收益率采用滞后一期的数据（杨雪莱），同时剔除
节假日不一致导致的非重合部分。共得到3215组数据。之后将2007年1月1日至2009年12月31
日为第一阶段，2010年1月1日至2017年8月13日为第二阶段，（为什么这样划分，有没有依
据）。2017年8月14日,特朗普签署行政备忘录，指示对中国发起贸易调查，因此将2017年8
月14日之后的时间划分为第三阶段，表示中美贸易战的持续时间。在进行实证研究之前，首
先分别对沪深300指数和标准普尔指数计算对数收益率，并进行统计检验。



#### 收益率序列的描述分析及统计检验

<div class="figure" style="text-align: center">
<img src="03-estimation_files/figure-html/norm_test_rtn-1.png" alt="概率密度曲线图" width="60%" />
<p class="caption">(\#fig:norm_test_rtn)概率密度曲线图</p>
</div>

```
##      
## 0.01
```

```
## Dickey-Fuller 
##     -12.85665
```

```
## 
## Title:
##  Augmented Dickey-Fuller Test
## 
## Test Results:
##   PARAMETER:
##     Lag Order: 16
##   STATISTIC:
##     Dickey-Fuller: -5.1027
##   P VALUE:
##     0.01 
## 
## Description:
##  Sat Nov 28 20:46:35 2020 by user: LENOVO
```

```
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 384.68, df = 1, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 891.2, df = 2, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 890.99, df = 3, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 922.53, df = 4, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 1006.8, df = 5, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 1029.9, df = 6, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 1030.4, df = 7, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 1030.1, df = 8, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 1037.2, df = 9, p-value < 2.2e-16
## 
## 
## 	ARCH LM-test; Null hypothesis: no ARCH effects
## 
## data:  gspc.rtn$return
## Chi-squared = 1036.8, df = 10, p-value < 2.2e-16
```


```r
gspc_return=c(round(basicStats(gspc.rtn$return)[3:4,],3),round(basicStats(gspc.rtn$return)[7,],5),round(basicStats(gspc.rtn$return)[14:16,],3),paste(as.character(round(normalTest(gspc.rtn$return)@test$statistic,3)),'***'),paste(as.character(round(adfTest(gspc.rtn$return,lag=16,type='nc')@test$statistic),3),'***'),paste(as.character(round(ArchTest(gspc.rtn$return,lag=10)$statistic),3),'***'))
rmbcsi_return=c(round(basicStats(rmbcsi.rtn$return)[3:4,],3),round(basicStats(rmbcsi.rtn$return)[7,],5),round(basicStats(rmbcsi.rtn$return)[14:16,],3),paste(as.character(round(normalTest(rmbcsi.rtn$return)@test$statistic,3)),'***'),paste(as.character(round(adfTest(rmbcsi.rtn$return,lag=16,type='nc')@test$statistic),3),'***'),paste(as.character(round(ArchTest(rmbcsi.rtn$return,lag=10)$statistic),3),'***'))
colname=c('最小值','最大值','均值','标准差','偏度','峰度','J-B','ADF','ARCH')
return_desc=data.frame(rbind(t(rmbcsi_return),t(gspc_return)))
colnames(return_desc)=colname
rownames(return_desc)=c('rmbcsi_return','gspc_return')
library("kableExtra")
knitr::kable(return_desc, row.names =T, align = c("l", "c", "c", "c"),
             caption="收益率序列描述统计表",digits = 3,
             longtable = TRUE, booktabs = TRUE, escape = F, linesep = "") %>%
    kable_styling(full_width = T) %>%
    kable_styling(latex_options = c("striped", "scale_down", "repeat_header", "hold_position"),
                  ## stripe_index = rep(1:5, 4)+rep(c(0,10,20,30), each = 5),
                  repeat_header_text = "(续)")%>%
    column_spec(1:6, width = c("1.5cm"))%>%
    footnote(general ="J-B为$Jarque Bera$的正态分布检验;ADF检验采用无趋势项、无常数项形式;ARCH效应检验的滞后阶数为10;***显著性水平1%;**显著性水平5%;*显著性水平10%",general_title = "注:",footnote_as_chunk = T)
```

<table class="table table" style="margin-left: auto; margin-right: auto; margin-left: auto; margin-right: auto;">
<caption>(\#tab:describe_rtn)收益率序列描述统计表</caption>
 <thead>
  <tr>
   <th style="text-align:left;">   </th>
   <th style="text-align:left;"> 最小值 </th>
   <th style="text-align:center;"> 最大值 </th>
   <th style="text-align:center;"> 均值 </th>
   <th style="text-align:center;"> 标准差 </th>
   <th style="text-align:left;"> 偏度 </th>
   <th style="text-align:center;"> 峰度 </th>
   <th style="text-align:center;"> J-B </th>
   <th style="text-align:center;"> ADF </th>
   <th style="text-align:left;"> ARCH </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;width: 1.5cm; "> rmbcsi_return </td>
   <td style="text-align:left;width: 1.5cm; "> -0.097 </td>
   <td style="text-align:center;width: 1.5cm; "> 0.089 </td>
   <td style="text-align:center;width: 1.5cm; "> 0.00026 </td>
   <td style="text-align:center;width: 1.5cm; "> 0.018 </td>
   <td style="text-align:left;width: 1.5cm; "> -0.535 </td>
   <td style="text-align:center;"> 3.811 </td>
   <td style="text-align:center;"> 0.94 *** </td>
   <td style="text-align:center;"> -13 *** </td>
   <td style="text-align:left;"> 328 *** </td>
  </tr>
  <tr>
   <td style="text-align:left;width: 1.5cm; "> gspc_return </td>
   <td style="text-align:left;width: 1.5cm; "> -0.128 </td>
   <td style="text-align:center;width: 1.5cm; "> 0.11 </td>
   <td style="text-align:center;width: 1.5cm; "> 0.00025 </td>
   <td style="text-align:center;width: 1.5cm; "> 0.013 </td>
   <td style="text-align:left;width: 1.5cm; "> -0.515 </td>
   <td style="text-align:center;"> 13.116 </td>
   <td style="text-align:center;"> 0.853 *** </td>
   <td style="text-align:center;"> -14 *** </td>
   <td style="text-align:left;"> 1037 *** </td>
  </tr>
</tbody>
<tfoot><tr><td style="padding: 0; border: 0;" colspan="100%">
<span style="font-style: italic;">注:</span> <sup></sup> J-B为$Jarque Bera$的正态分布检验;ADF检验采用无趋势项、无常数项形式;ARCH效应检验的滞后阶数为10;***显著性水平1%;**显著性水平5%;*显著性水平10%</td></tr></tfoot>
</table>

表\@ref(tab:describe_rtn) 是中国沪深300指数(rmbcsi_return)和美国标准普尔500指数
(gspc_return)的基本统计特征，两国的收益率均值都大于0，gspc_return的变动范围要大
于rmbcsi_return，美股收益率的标准差小于沪深300指数的收益率，通过概率密度曲线图
\@ref(fig:norm_test_rtn)可以直观的看出，虽然二者都呈现呈尖峰厚尾的特征，并且J-B
统计量的检验结果也显示，两国股市收益率不服从正态分布，但右图标准普尔500指数收益
率的分布要更为陡峭，相应表\@ref(tab:describe_rtn)中的峰度系数也证实了这一点。对
rmbcsi_return和gspc_return进行ADF单位根检验和ARCH效应检验显示，收益率序列是平稳
的且存在显著的波动聚集效应。




<!-- ```
rowname=gspc.rtn$date
gspc.rtn=data.frame(gspc.rtn[,-1],row.names=rowname)
colnames(gspc.rtn)='return'
rmbcsi.rtn=data.frame(rmbcsi.rtn[,-1],row.names=rowname)
colnames(rmbcsi.rtn)='return'
plot(as.timeSeries(gspc.rtn),lty=1,col="blue")
lines(as.timeSeries(rmbcsi.rtn),lty=2,col="red")
``` -->

<div class="figure" style="text-align: center">
<img src="03-estimation_files/figure-html/fig_rtn_cepu-1.png" alt="股市收益率与EPU趋势图" width="60%" />
<p class="caption">(\#fig:fig_rtn_cepu)股市收益率与EPU趋势图</p>
</div>

图 \@ref(fig:fig_rtn_cepu) 分别展现了中美两国股市收益率与EPU的趋势图。从左图中可
以看出，样本期间，中国股市主要有三个较为集中的波动阶段，分别为2008年至2009年的金
融危机，2015年至2016年以及2019年前后。而对应中国EPU在这三个阶段也出现了波动峰
点。金融危机期间，为缓解金融危机对我国经济的影响，政府出台了以４万亿为首的一系列
强经济刺激政策，在之后的2011年至2012年，这些措施的负面影响开始显现，并且我国正经
历换届选举，导致当时的EPU出现较大波动；2015年，以场外资金为首引发的股市大幅震
荡，以及外界对中国继续保持7%的经济增速持悲观态度，股市的“疯狂”和外界的市场担忧导
致中国EPU再次走高；2017年开始的贸易战在2019年达到高潮，股市和EPU都对应出现了再次
波动，但波动幅度较之前两次较小。另外，中国股市与EPU均在2018年期间相对保持平稳。
以上所述表明，中国股市与EPU大致保持相似的趋势变化。这种对应关系同样存在于美国。
从右图中可知，对应美国2008至2010年的金融危机期间，伴随着经济刺激计划，其EPU也出
现了明显的波动，股市和EPU波动均出现峰值；2012年底，美国身陷“财政悬崖”，股市也出
现小范围波动；美国股市和政府对关税和贸易政策紧张局势加剧的反应也均有反映，这体现
在2019至出现的较大幅度振荡。基于此，本文接下来将通过建模，定量研究股市与EPU之间
的关系。




## 经济政策不确定性对沪深300指数波动率影响研究

### 基于GARCH-MIDAS模型的沪深300指数波动率模型构建


```r
#数据准备
library(mfGARCH)
#install.packages('mfGARCH')
#help(package="mfGARCH")

year_month=vector(length=length(date))
cepu=vector(length=length(date))
RT=vector(length=length(date))
RT_rolling=vector(length=length(date))
for(i in 1:length(date)){
   year_month[i]=paste(substring(as.character(rmbcsi.rtn$date[i]),1,7),'01',sep='-')}

freq=data.frame(table(year_month))
cepu=rep(huang_cepu$CNEPU[85:248],times=as.vector(freq$Freq))
dcepu=rep(diff(huang_cepu$CNEPU[84:248]),times=as.vector(freq$Freq))
RT[1:freq$Freq[1]]=sum((rmbcsi.rtn$return[1:freq$Freq[1]])^2)
for(i in 2:length(freq$year_month)){
    t=sum(freq$Freq[1:(i-1)])
    RT[(t+1):(t+freq$Freq[i])]=sum((rmbcsi.rtn$return[(t+1):(t+freq$Freq[i])])^2)
}
n=round(mean(freq$Freq),0)
RT_rolling[1]=rmbcsi.rtn$return[1]
for(i in 2:length(date)){
    if(i<=(n+1)){
        RT_rolling[i]=mean((rmbcsi.rtn$return[1:(i-1)])^2)
    }
    else{
        RT_rolling[i]=mean((rmbcsi.rtn$return[(i-n):(i-1)])^2)
    } 
    }
data_rmbcsi_rtn=data.frame(cbind(cbind(cbind(rmbcsi.rtn,RT),cepu),year_month))
#data_rmbcsi_rtn[,c(1,5)]=apply(data_rmbcsi_rtn[,c(1,5)],2,function(x) as.Date(x,format='%Y-%m-%d'))
data_rmbcsi_rtn$year_month=as.Date(data_rmbcsi_rtn$year_month,format='%Y-%m-%d')
data_rmbcsi_rtn$date=as.Date(data_rmbcsi_rtn$date,format='%Y-%m-%d')
data_rmbcsi_rtn$return=data_rmbcsi_rtn$return*100
data_rmbcsi_rtn$dcepu=dcepu
data_rmbcsi_rtn$RT_rolling=RT_rolling
#write.csv(data_rmbcsi_rtn,'./thesis/results/data_rmbcsi_rtn.csv')
#paste函数，sep表示参数之间的分隔符,collapse表示消除两个字符之间的空间
#head(data_rmbcsi_rtn)
#head(df_mfgarch)
fit_mfgarch(data = df_financial, y = "return", x = "nfci", low.freq = "week", K
= 52)


#关于两个协变量，仅实现非对称GJR-GARCH分量
##1 RT与cepu的滞后项均为12
model_two=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq ="year_month", K = 12,x.two = "cepu", K.two = 12, low.freq.two = "year_month",weighting.two = "beta.restricted")
# x.two = "cepu";gamma=FALSE
# is.null(x.two)==FALSE && gamma==FALSE
##2 RT滞后项为6，cepu滞后项为12
model_two_k6=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 6,x.two = "cepu", K.two = 12, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##3 RT滞后项为18，cepu滞后项为12
model_two_k18=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 18,x.two = "cepu", K.two = 12, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##4 RT滞后项为18，cepu滞后项为18
model_two_kk18=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 18,x.two = "cepu", K.two = 18, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##5 RT滞后项为18，cepu滞后项为6
model_two_k118_k26=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 18,x.two = "cepu", K.two = 6, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##d1 RT滞后项为18，dcepu滞后项为12,theta和theta.two都不显著
model_two_dk18=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 18,x.two = "dcepu", K.two = 12, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##6 RT滞后项为18，cepu滞后项为10
model_two_k118_k2_10=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 18,x.two = "cepu", K.two = 10, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##7 RT滞后项为24，cepu滞后项为12
model_two_k1_24_k2_12=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 24,x.two = "cepu", K.two = 12, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##8 RT滞后项为30，cepu滞后项为12
model_two_k1_30_k2_12=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 30,x.two = "cepu", K.two = 12, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##d2 RT滞后项为18，dcepu滞后项为6,两个theta都不显著
model_two_k1_18_dk2_6=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 18,x.two = "dcepu", K.two = 6, low.freq.two =
"year_month",weighting.two = "beta.restricted")
##d3 RT滞后项为12，dcepu滞后项为6，两个theta仍然不显著
model_two_k1_12_dk2_6=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 12,x.two = "dcepu", K.two = 6, low.freq.two =
"year_month",weighting.two = "beta.restricted")
## d4 RT滞后项为12，dcepu滞后项为12
model_two_k1_12_dk2_12=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 12,x.two = "dcepu", K.two = 12, low.freq.two =
"year_month",weighting.two = "beta.restricted")
## d5 RT滞后项为12，dcepu滞后项为18
model_two_k1_12_dk2_18=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 12,x.two = "dcepu", K.two = 18, low.freq.two =
"year_month",weighting.two = "beta.restricted")
## d6 RT滞后项为6，dcepu滞后项为12
model_two_k1_6_dk2_12=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 6,x.two = "dcepu", K.two = 12, low.freq.two =
"year_month",weighting.two = "beta.restricted")

## d7 RT滞后项为6，dcepu滞后项为6
model_two_k1_6_dk2_6=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 6,x.two = "dcepu", K.two = 6, low.freq.two =
"year_month",weighting.two = "beta.restricted")

model_two_k1_6_dk2_10=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 6,x.two = "dcepu", K.two = 10, low.freq.two =
"year_month",weighting.two = "beta.restricted")

model_two_k1_6_dk2_4=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
="year_month", K = 6,x.two = "dcepu", K.two = 4, low.freq.two =
"year_month",weighting.two = "beta.restricted")

model_rt=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "RT",low.freq
= "year_month", K = 12,weighting.two = "beta.restricted")

model_cepu=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "cepu",
low.freq = "year_month", K = 12,weighting.two = "beta.restricted")

model_dcepu=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x = "dcepu",
low.freq = "year_month", K = 12,weighting.two = "beta.restricted")
resid=model_two_k18$residuals
plot_weighting_scheme(model_two_k18)
plot(x=seq(1,18),y=model_two_k18$est.weighting,lty=1)
plot(model_two_k18$est.weighting.two,pch=8)
head(model_two_k18$est.weighting.two)


#关于是否含有外生变量，在《引入经济政策不确定性指标的沪深300指数已实现波动率建模
#研究》中，使用5分钟股票数据与月度和日度的GARCH-MIDAS模型，只引入一个外生变量；在
#夏婷的《经济不确定性是股市波动的因子吗》中，分别构建rv,rv+cepu,rv+aepu,RV+IP(工
#业增长率),RV+INF(通货率),RV+IR(同业拆借利率)；在艾龙的文章中，引入控制变量;在雷
#立坤的文章中，也只有股市和epu两个变量
```

```r
str(model_two)
model_two$llh#极大似然值-5329.677
model_two$bic#BIC10731.35
model_two_k6

model_two_k18$llh#-5024.294,RT的滞后期为18,cepu为12
model_two_k18$bic#10120.23
model_two_k1_24_k2_12$llh#-4714.129
model_two_k1_24_k2_12$bic#9499.51
model_two_k1_30_k2_12$llh#-4474.528
model_two_k1_30_k2_12$bic#9019.926

model_two_kk18$llh#非常不好,k18的模型因为rt滞后期更长，拟合效果好，kk18中epu的滞
#后期为18，效果最差，说明RT要长，EPU要短,但是当epu滞后为6时，rt系数不显著;当epu滞
#后10，rt仍然不显著；虽然当RT的滞后期设为24和30以后,LLH和BIC变的良好，但theta系
#数不再显著，因此RT为18,CEPU位12时模型最好.用CEPU的差分效果不好

model_rt$bic#10721.65
model_rt$llh
model_cepu$bic#10720.03
model_cepu$llh
model_dcepu$bic#10712.56
model_dcepu$llh
model_two_kk18$bic#10127.1
model_two_k118_k26$llh#-5028.075
model_two_k118_k26$bic#10127.79
model_two_k118_k2_10$llh#-5026.947
model_two_k118_k2_10$bic#10125.54
model_two_dk18$llh#-5024.861
model_two_dk18$bic#10121.36
model_two_k1_12_dk2_6$bic#10736.37
model_two_k1_18_dk2_6$bic#10134.73
model_two_k1_12_dk2_12$bic#10724.6
model_two_k1_12_dk2_18
model_two_k1_6_dk2_6$llh#-5594.098
model_two_k1_6_dk2_10
model_two_k1_6_dk2_4
```

从某种意义上说，DCC MIDAS是数据驱动的模型。 该软件确实报告了估计器协方差矩阵
（EstParamCov），其中对角元素的平方根是参数的标准误差。 该软件还报告条件相关矩
阵。 该模型也适用于波动率预测.
在526行，有DCC-MIDAS的FML可以添加外生变量
在GARCH-MIDAS中做预测时，通过There is a name/value pair 可以指定预测时的外生变
量，默认使用已实现波动率
### 沪深300指数波动率模型比较及最优模型选择

### 模型结果的解释分析

## 经济政策不确定性对道琼斯工业指数波动率影响研究




```r
model_k2_0=fit_mfgarch(data = data_gspc_rtn, y = "return", x = "RT_usa",low.freq
="year_month", K = 12,x.two = "aepu", K.two = 1, low.freq.two =
"year_month",weighting.two = "beta.restricted")
model_k2_0$bic#8039.633
results_rv_rolling2[[1]]#12,24,theta.two,w2.two不显著
results_rv_rolling2[[2]]#18,24,m,theta.two,w2.two不显著
results_rv_rolling2[[3]]#24,24,m,theta.two,w2.two不显著
results_rv_rolling2[[4]]#12,26,m,theta.two,w2.two不显著
results_rv_rolling2[[5]]#18,26,m,theta.two,w2.two不显著
results_rv_rolling2[[6]]#24,26,m,theta.two,w2.two不显著
results_rv_rolling2[[7]]#12,28,theta.two,w2.two不显著
results_rv_rolling2[[8]]#18,28,theta.two,w2.two不显著
results_rv_rolling2[[9]]#24,28,m,theta.two,w2.two不显著
results_rv_rolling2[[10]]#12,30,m,theta.two,w2.two不显著
results_rv_rolling2[[11]]#12,30,m,theta.two,w2.two不显著
results_rv_rolling2[[12]]#24,30,m,theta.two,w2.two不显著

cor(x=as.vector(data_gspc_rtn$aepu),y=as.vector(data_gspc_rtn$RT_usa),method="pearson")
#colnames(results_rv_rolling_data_frame2)=colnames(results_rv_rolling_data_frame)
#results_rv_rolling_data_frame_t=rbind(results_rv_rolling_data_frame,results_rv_rolling_data_frame2)
#results_rv_rolling_data_frame_t[order(results_rv_rolling_data_frame_t$k_one),]
```
### 基于GARCH-MIDAS模型的沪深300指数波动率模型构建

### 沪深300指数波动率模型比较及最优模型选择

### 模型结果的解释分析



<!--# 参考文献 {-}-->
[//]: # (\bibliography{Bibfile})
