
################# 第 3 章 R 程序代码  ####################

knitr::opts_knit$set(root.dir = getwd())
knitr::opts_chunk$set(echo = FALSE, results = 'hide',cache=T)
knitr::opts_chunk$set(warning = FALSE, message=FALSE)
knitr::opts_chunk$set(fig.align="center"
                      ## ,out.width="0.9\\textwidth" # latex
                      ,out.width="80%" # for both latex and html
                      ,fig.width=5, fig.height=3
                      )

rm(list=ls())
options(digits=4)
options(scipen=100)
graphics.off()
Sys.setlocale("LC_ALL", "Chinese")

library(xlsx)
library(psych)
library(fBasics)
library(FinTS)
library(fUnitRoots)
library(timeSeries)

steven_cepu=read.xlsx("./data/steven_EPU.xlsx",3,colIndex=c(1,2,3),encoding = "UTF-8")
baker_cepu=read.xlsx('./data/baker_Data.xlsx',1,encoding = "UTF-8")
huang_cepu=read.xlsx('./data/cnepu_1_november_2020_updated.xlsx',1,encoding ="UTF-8")
aepu=read.xlsx('./data/US_Policy_Uncertainty_Data.xlsx',1,encoding='UTF-8')
rmbcsi=read.xlsx("./data/rmbcsi.xls",1,encoding='UTF-8')#2007年1月4日
gspc=read.csv("./data/GSPC.csv")#2007年1月3日
data_rmbcsi_rtn=read.xlsx('./results/data_rmbcsi_rtn_new.xlsx',1,encoding='UTF_8')
data_gspc_rtn=read.xlsx('./results/data_gspc_rtn_new.xlsx',1,encoding='UTF_8')

# rmbcsi_table_result=read.csv('./thesis/results/rmbcsi_table_result_0307.csv')
# data_rmbcsi_rtn=read.xlsx('./thesis/results/data_rmbcsi_rtn_new.xlsx',1,encoding='UTF_8')

# gspc=read.csv("./thesis/data/GSPC.csv")#2007年1月3日
# rmbcsi=read.xlsx("./thesis/data/rmbcsi.xls",1,encoding='UTF-8')#2007年1月4日
# aepu=read.xlsx('./thesis/data/US_Policy_Uncertainty_Data.xlsx',1,encoding='UTF-8')
# steven_cepu=read.xlsx("./thesis/data/steven_EPU.xlsx",3,colIndex=c(1,2,3),encoding = "UTF-8")
# baker_cepu=read.xlsx('./thesis/data/baker_Data.xlsx',1,encoding = "UTF-8")
# huang_cepu=read.xlsx('./thesis/data/cnepu_1_november_2020_updated.xlsx',1,encoding= "UTF-8")

# data_gspc_rtn=read.xlsx('./thesis/results/data_gspc_rtn_new.xlsx',1,encoding='UTF_8')
# source('./thesis/model.r')
#rmbcsi_table_result=read.csv('./thesis/results/rmbcsi_table_result_1230.csv')
rmbcsi_table_result=read.csv('./results/rmbcsi_table_result_0307.csv')
#source('./thesis/model.r')
gspc_table_result=read.csv('./results/gspc_table_result_1230.csv')
#gspc_table_result=read.csv('./thesis/results/gspc_table_result_1230.csv')

par(mar=c(2,2,1,1))
scepu=ts(steven_cepu$EPU,frequency=12,start=c(2000,1),end=c(2020,12))
bcepu=ts(baker_cepu$China.News.Based.EPU[as.numeric(as.character(baker_cepu$year))>=2000],frequency=12,start=c(2000,1),end=c(2020,12))
hcepu=ts(huang_cepu$CNEPU,frequency=12,start=c(2000,1),end=c(2020,12))
plot(hcepu,ylab='China EPU',lty=3,ylim=c(0,1000))
lines(scepu,lty=1)
lines(bcepu,lty=2)
legend('topleft',legend=c('CEPU1','CEPU2','CEPU3'),lty=c(3,2,1),cex=0.7)

library("kableExtra")
#cepu_desc=read.csv('./thesis/results/cepu_desc.csv')
cepu_desc=read.csv('./results/cepu_desc.csv')
colnames(cepu_desc)=c("术语","CEPU1","CEPU2","CEPU3")
knitr::kable(cepu_desc,row.names =F, align = c("l", "l", "l", "l"),caption="中国经济政策不确定性指数描述统计",
      longtable = TRUE, booktabs = TRUE,escape = F,
      linesep=c(rep("",2), "\\hline"))%>%
    kable_styling(latex_options = c("striped", "scale_down", "repeat_header", "hold_position"),
                  ## stripe_index = rep(1:5, 4)+rep(c(0,10,20,30), each = 5),
                  repeat_header_text = "(续)")%>%
    kable_styling(full_width = T)%>%
    column_spec(1:4,width=c("2cm","4cm","4cm","4cm"))

par(mar=c(2,2,1,1))
aepu_news=ts(aepu$News_Based_Policy_Uncert_Index[181:432],start=c(2000,1),frequency=12)
aepu_three=ts(aepu$Three_Component_Index[181:432],start=c(2000,1),frequency=12)
plot(aepu_news,lty=1)
lines(aepu_three,lty=2)
legend('topleft',legend=c('AEPU1','AEPU2'),lty=c(1,2),cex=0.7)

#数据处理
gspc$Date=as.character(as.Date(gspc$Date))
gspc.rtn=diff(log(gspc$Adj.Close))
gspc.rtn=data.frame(gspc$Date[2:length(gspc$Date)],gspc.rtn)
colnames(gspc.rtn)=c('date','return')

rmbcsi_order=rmbcsi[order(rmbcsi$日期),]
#head(rmbcsi_order)
rmbcsi.rtn=diff(log(rmbcsi_order$收盘点位))
rmbcsi.rtn=data.frame(rmbcsi_order$日期[2:length(rmbcsi_order$日期)],rmbcsi.rtn)
colnames(rmbcsi.rtn)=c('date','return')
#去掉非重合部分
date = intersect(gspc.rtn$date,rmbcsi.rtn$date)
length(date)#3293
gspc.rtn=gspc.rtn[gspc.rtn$date %in% date,]
rmbcsi.rtn=rmbcsi.rtn[rmbcsi.rtn$date %in% date,]

# return.desc=read.csv('./thesis/results/return_desc.csv')
return.desc=read.csv('./results/return_desc.csv')
return.desc[,1]=c("CSI","GSPC")
library("kableExtra")
knitr::kable(return.desc, row.names =F, align = c("l", "c", "c", "c"),
             caption="收益率序列描述统计表",digits = 4,
             longtable = TRUE, booktabs = TRUE, escape = F, linesep = "") %>%
    kable_styling(full_width = T) %>%
    kable_styling(latex_options = c("scale_down", "repeat_header", "hold_position"),
                  ## stripe_index = rep(1:5, 4)+rep(c(0,10,20,30), each = 5),
                  repeat_header_text = "(续)")%>%
    column_spec(1:6, width = c("1cm","2cm","2cm","2.5cm","3cm","3cm"))%>%
    footnote(general ="***显著性水平1%;**显著性水平5%;*显著性水平10%",
    general_title = "注:",footnote_as_chunk = T)

par(mfrow=c(1,2),mar=c(1,2,0,1))
#hist(gspc.rtn,nclass=30,freq = FALSE) 
# Histogram直方图break用于指定分组,freq=false表示根据概率密度而不是频数绘制图形
# 当对象都是标量时,breaks和nclass作业相同
d2=density(rmbcsi.rtn$return)  # Obtain density estimate
x2=seq(range(rmbcsi.rtn$return)[1], range(rmbcsi.rtn$return)[2],by = ((range(rmbcsi.rtn$return)[2]-range(rmbcsi.rtn$return)[1])/500))
y2=dnorm(x2,mean(rmbcsi.rtn$return),stdev(rmbcsi.rtn$return)) # 计算正态分布的概率密度
plot(d2$x,d2$y,xlab='rmbcsi_rtn',ylab='density',type='l',ylim=c(0,60))
lines(x2,y2,lty=2) #same mean, same stdev
legend('topleft',legend=c('CSI','normal'),lty=c(1,2),cex=0.5)

d1=density(gspc.rtn$return)  # Obtain density estimate
x=seq(range(gspc.rtn$return)[1], range(gspc.rtn$return)[2],by = ((range(gspc.rtn$return)[2]-range(gspc.rtn$return)[1])/500))
y1=dnorm(x,mean(gspc.rtn$return),stdev(gspc.rtn$return)) # 计算正态分布的概率密度
plot(d1$x,d1$y,xlab='gspc_rtn',ylab='density',type='l',ylim=c(0,60))
lines(x,y1,lty=2) #same mean, same stdev
legend('topleft',legend=c('GSPC','normal'),lty=c(1,2),cex=0.5)

par(mfrow=c(2,1))
par(mar=c(0,4,2,1))
plot(rmbcsi.rtn$return,type="l",xaxt='n',ylab="CSI",yaxt='n',cex=0.8)
axis(2,c(-0.05,0.05))
par(mar=c(2,4,0.5,1))
plot(gspc.rtn$return,type="l",xaxt='n',ylab="GSPC",xlab="Time",cex=0.8)
axis(1,c(1,235,473,709,948,1180,1413,1643,1880,2116,2352,2589,2824,3060,3293),labels=c('2007-01','2008-01','2009-01','2010-01','2011-01','2012-01','2013-01','2014-01','2015-01','2016-01','2017-01','2018-01','2019-01','2020-01','2020-12'))

par(mfrow=c(2,1))
par(mar=c(0,4,2,1))
plot(huang_cepu$CNEPU[85:248],xaxt='n',type='l',ylab="CEPU",cex.axis=1)
par(mar=c(2,4,0.5,1))
plot(aepu_three[85:248],xaxt='n',type='l',ylab="AEPU",xlab="Time")
axis(1,c(1,seq(12,168,12)),labels=c('2007-01','2008-01','2009-01','2010-01','2011-01','2012-01','2013-01','2014-01','2015-01','2016-01','2017-01','2018-01','2019-01','2020-01','2020-12'))

library("kableExtra")
knitr::kable(rmbcsi_table_result, row.names =F, align = c("l"),
             caption="沪深300指数GARCH-MIDAS模型估计",
             longtable = TRUE, booktabs = TRUE, escape = F, linesep = "") %>%
    kable_styling(full_width = T) %>%
    kable_styling(latex_options = c("striped", "scale_down", "repeat_header", "hold_position"),
                  repeat_header_text = "(续)")%>%
    column_spec(1:7, width = c("1cm",rep("2cm",6)))%>%
    footnote(general ="***显著性水平1%;**显著性水平5%;*显著性水平10%",general_title = "注:",footnote_as_chunk = T)

# rmbcsi_weight18=read.csv('./thesis/results/rmbcsi_weight18.csv')
# weight_aepu24=read.csv('./thesis/results/weight_aepu24.csv')
# weight_cepu36=read.csv('./thesis/results/weight_cepu36.csv')
rmbcsi_weight18=read.csv('./results/rmbcsi_weight18.csv')
weight_aepu24=read.csv('./results/weight_aepu24.csv')
weight_cepu36=read.csv('./results/weight_cepu36.csv')
par(mfrow=c(3,1),mar=c(2,4,0.5,2))
plot(rmbcsi_weight18,type='l',ylab="weight")
legend('topright',legend='RV,k=18',cex=0.7)
plot(weight_cepu36,type='l',ylab="weight")
legend('topright',legend='CEPU,k=36',cex=0.7)
plot(weight_aepu24,type='l',ylab="weight")
legend('topright',legend='AEPU,k=24',cex=0.7)

rmbcsi_weight1_k1_18_K2_36=read.csv('./results/rmbcsi_weight1_k1_18_K2_36.csv')
rmbcsi_weight2_k1_18_K2_36=read.csv('./results/rmbcsi_weight2_k1_18_K2_36.csv')
rmbcsi_weight1_k1_18_K2_24=read.csv('./results/rmbcsi_weight1_k1_18_K2_24.csv')
rmbcsi_weight2_k1_18_K2_24=read.csv('./results/rmbcsi_weight2_k1_18_K2_24.csv')
 weight_cmodel_k1_24_k2_24=read.csv('./results/weight_cmodel_k1_24_k2_24.csv')
# rmbcsi_weight1_k1_18_K2_36=read.csv('./thesis/results/rmbcsi_weight1_k1_18_K2_36.csv')
# rmbcsi_weight2_k1_18_K2_36=read.csv('./thesis/results/rmbcsi_weight2_k1_18_K2_36.csv')
# rmbcsi_weight1_k1_18_K2_24=read.csv('./thesis/results/rmbcsi_weight1_k1_18_K2_24.csv')
# rmbcsi_weight2_k1_18_K2_24=read.csv('./thesis/results/rmbcsi_weight2_k1_18_K2_24.csv')

# rmbcsi_tau_k24=read.csv('./thesis/results/rmbcsi_tau_k24.csv')
# rmbcsi_tau18=read.csv('./thesis/results/rmbcsi_tau18.csv')
# tau_cepu12=read.csv('./thesis/results/tau_cepu12.csv')
# rmbcsi_tau_k1_18_K2_12=read.csv('./thesis/results/rmbcsi_tau_k1_18_K2_12.csv')
# rmbcsi_tau_k1_18_K2_24=read.csv('./thesis/results/rmbcsi_tau_k1_18_K2_24.csv')
# weight_cmodel_k1_24_k2_24=read.csv('./thesis/results/weight_cmodel_k1_24_k2_24.csv')

# tau_k1_18_k2_36 =read.csv('./thesis/results/rmbcsi_tau_k1_18_K2_36.csv')
# g_k1_18_K2_36=read.csv('./thesis/results/rmbcsi_g_k1_18_K2_36.csv')
tau_k1_18_k2_36=read.csv('./results/rmbcsi_tau_k1_18_K2_36.csv')
g_k1_18_K2_36=read.csv('./results/rmbcsi_g_k1_18_K2_36.csv')
par(mfrow=c(2,2),mar=c(2,2,1,1))
plot(rmbcsi_weight1_k1_18_K2_36,type='l')
legend('topright',legend='RV,k1=18')
plot(rmbcsi_weight2_k1_18_K2_36,type='l')
legend('topright',legend='CEPU,K2=36')
plot(rmbcsi_weight1_k1_18_K2_24,type='l')
legend('topright',legend='RV,k1=18')
plot(weight_cmodel_k1_24_k2_24,type='l')
legend('topright',legend='CEPU,K1=24')

par(mar=c(2,2,1,1))
plot(g_k1_18_K2_36[720:3293,2],xaxt='n',type='l',ylab="",xlab="",lty=2)
axis(1,c(1,228,460,693,923,1160,1396,1632,1869,2104,2340,2573),labels=c('2010-01','2011-01','2012-01','2013-01','2014-01','2015-01','2016-01','2017-01','2018-01','2019-01','2020-01','2020-12'))
lines(tau_k1_18_k2_36[720:3293,2],lty=1,ylab="",xlab="")
lines(g_k1_18_K2_36[720:3293,2] * tau_k1_18_k2_36[720:3293,2],lty=3,ylab="",xlab="")
legend('topleft',legend=c('short-var','long-var','var'),lty=c(2,1,3),cex=0.7)

library("kableExtra") 
knitr::kable(gspc_table_result, row.names =F, align = c("l","l"),
             caption="标准普尔500指数GARCH-MIDAS模型估计",
             longtable = TRUE, booktabs = TRUE, escape = F, linesep = "") %>%
    kable_styling(full_width = T) %>%
    kable_styling(latex_options = c("striped", "scale_down", "repeat_header", "hold_position"),
                  ## stripe_index = rep(1:5, 4)+rep(c(0,10,20,30), each = 5),
                  repeat_header_text = "(续)")%>%
    column_spec(1:7, width = c("1cm",rep("2cm",6)))%>%
    footnote(general ="***显著性水平1%;**显著性水平5%;*显著性水平10%",
    general_title = "注:",footnote_as_chunk = T)

# weight_rt36=read.csv('./thesis/results/model_rt_k36_est.weighting.csv')
# weight_aepu18=read.csv('./thesis/results/model_aepu_k18_est.weighting.csv')
# weight1_k1_36_K2_18=read.csv('./thesis/results/model_k1_36_K2_18_weight1.csv')
# weight2_k1_36_K2_18=read.csv('./thesis/results/model_k1_36_K2_18_weight2.csv')
weight_rt36=read.csv('./results/model_rt_k36_est.weighting.csv')
weight_aepu18=read.csv('./results/model_aepu_k18_est.weighting.csv')

# g36=read.csv('./thesis/results/model_rt_k36_g.csv')
# tau36=read.csv('./thesis/results/model_rt_k36_tau.csv')
# tau_aepu18=read.csv('./thesis/results/model_aepu_k18_tau.csv')
# tau_two=read.csv('./thesis/results/model_two_tau.csv')
g36=read.csv('./results/model_rt_k36_g.csv')
tau36=read.csv('./results/model_rt_k36_tau.csv')

par(mfrow=c(1,2), mar=c(2.1, 2.1, 1.1, 1.1))
plot(weight_rt36,type='l')
legend('topright',legend=c('RV,K=36'),cex=0.7)
plot(weight_aepu18,type='l')
legend('topright',legend=c('AEPU,K=18'),cex=0.7)

par(mar=c(2,2,1,1))
plot(ts(g36[720:3293,2]),xaxt='n',type='l',ylim=c(0,20),ylab='',xlab='',lty=2)
#legend('topleft',legend=c('short var'))
lines(ts(tau36[720:3293,2]),type='l',xaxt='n',lty=1,ylab='',xlab='')
lines(ts(tau36[720:3293,2]*g36[720:3293,2]),type='l',lty=3,ylab='',xlab='')
axis(1,c(1,228,460,693,923,1160,1396,1632,1869,2104,2340,2573),labels=c('2010-01','2011-01','2012-01','2013-01','2014-01','2015-01','2016-01','2017-01','2018-01','2019-01','2020-01','2020-12'))
legend("topleft",legend=c("short var","long var",'var'),lty=c(2,1,3))
