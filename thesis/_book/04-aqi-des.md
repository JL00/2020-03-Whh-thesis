---
title: "第 4 章"
author: "Jin"
date: '2019-04-15'
institute: 中南财经政法大学统计与数学学院
csl: ./style/chinese-gb7714-2015-numeric.csl
css: ./style/markdown.css
bibliography: [./Bibfile.bib]
notice: '@*'
eqnPrefixTemplate: ($$i$$)
link-citations: true
linkReferences: true
chapters: true
tableEqns: false
autoEqnLabels: false
classoption: "aspectratio=1610"
---




# 经济政策不确定性对中美股票市场间关联性影响的研究

##  数据描述性统计

## 基于DCC-MIDAS模型的经济政策不确定性对中美股市关联性影响研究

为了对同业拆借市场的利率风险进行控制，可以借助“风险价值”（Value-at-Risk，VaR）来进行准
确测度。在险价值表示在某个置信水平下，由于市场的正常波动，金融资产的预计最大损失，
即收益率密度曲线的一个分位点。若 $VaR>r_t$，则说明该模型在第t天具备优良的表现，可以成
功预测。


<!--# 参考文献 {-}-->
[//]: # (\bibliography{Bibfile})
	
