---
title: "第 2 章"
author: "Jin"
date: '2019-04-15'
institute: 中南财经政法大学统计与数学学院
csl: ./style/chinese-gb7714-2015-numeric.csl
css: ./style/markdown.css
bibliography: [./Bibfile.bib]
notice: '@*'
eqnPrefixTemplate: ($$i$$)
link-citations: true
linkReferences: true
chapters: true
tableEqns: false
autoEqnLabels: false
classoption: "aspectratio=1610"
---







# 经济政策不确定性指标和混频模型简介

## 经济政策不确定性指标定义及测度

黄金期货市场属于金融资本交易场所，则意味着当取得投资回报时，也存在一定的市场风险。其风
险大致包含市价波动、流动性、市场信用、公开操作和法律方面。从而针对不同种类的风险，
就对应着不一样的影响因素。

### 经济政策不确定性指标的定义

### 经济政策不确定性指标的测度

## 经济政策不确定性对股票市场的影响机制

### 经济政策不确定性对股票市场波动率的影响机制

### 经济政策不确定性对中美股票市场关联性的影响机制

## 基于混频数据的GARCH-MIDAS模型和DCC-MIDAS模型介绍

### 广义自回归条件异方差混频数据抽样模型(GARCH-MIDAS)理论基础

### 混频数据抽样动态条件相关系数模型(DCC-MIDAS)理论基础


<!-- # 参考文献 {-} -->
<!--[//]: # (\bibliography{Bibfile})-->
	
