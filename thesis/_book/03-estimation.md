---
title: "第 3 章"
author: "Jin"
date: '2019-04-15'
institute: 中南财经政法大学统计与数学学院
csl: ./style/chinese-gb7714-2015-numeric.csl
css: ./style/markdown.css
bibliography: [./Bibfile.bib]
notice: '@*'
eqnPrefixTemplate: ($$i$$)
link-citations: true
linkReferences: true
chapters: true
tableEqns: false
autoEqnLabels: false
classoption: "aspectratio=1610"
---






# 经济政策不确定性对中美股票市场波动率影响的研究

## 数据选取和描述性统计

### 股票市场数据选取和变量预处理

### 收益率序列和经济政策不确定性指数的描述性分析及检验

不同于国外期货的选取，以最近月份的期货合约逐日交易的收盘价为基础，因为期货合约是有生命周期的，合约在
最后一个交易日将会平仓终止买卖。

## 经济政策不确定性对沪深300指数波动率影响研究

### 基于GARCH-MIDAS模型的沪深300指数波动率模型构建

### 沪深300指数波动率模型比较及最优模型选择

### 模型结果的解释分析

## 经济政策不确定性对道琼斯工业指数波动率影响研究

### 基于GARCH-MIDAS模型的沪深300指数波动率模型构建

### 沪深300指数波动率模型比较及最优模型选择

### 模型结果的解释分析



<!--# 参考文献 {-}-->
[//]: # (\bibliography{Bibfile})
