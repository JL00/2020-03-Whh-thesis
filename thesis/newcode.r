rm(data_rmbcsi_rtn)
rm(rmbcsi)
rmbcsi=read.xlsx("./thesis/data/rmbcsi.xls",1,encoding='UTF-8',endRow=3326)#2007年1月4日
rmbcsi_order=rmbcsi[order(rmbcsi$日期),]
rmbcsi.rtn=diff(log(rmbcsi_order$收盘点位))
rmbcsi.rtn=data.frame(rmbcsi_order$日期[2:length(rmbcsi_order$日期)],rmbcsi.rtn)
colnames(rmbcsi.rtn)=c('date','return')
date=intersect(gspc.rtn$date,rmbcsi.rtn$date)
gspc.rtn=gspc.rtn[gspc.rtn$date %in% date,]
rmbcsi.rtn=rmbcsi.rtn[rmbcsi.rtn$date %in% date,]
rmbcsi.rtn$return=rmbcsi.rtn$return*100

year_month=vector(length=length(date))
cepu=vector(length=length(date))
RT=vector(length=length(date))
RT_rolling=vector(length=length(date))
for(i in 1:length(date)){
   year_month[i]=paste(substring(as.character(rmbcsi.rtn$date[i]),1,7),'01',sep='-')}

freq=data.frame(table(year_month))
cepu=rep(huang_cepu$CNEPU[85:248],times=as.vector(freq$Freq))
dcepu=rep(diff(huang_cepu$CNEPU[84:248]),times=as.vector(freq$Freq))
RT[1:freq$Freq[1]]=mean((rmbcsi.rtn$return[1:freq$Freq[1]])^2)
for(i in 2:length(freq$year_month)){
    t=sum(freq$Freq[1:(i-1)])
    RT[(t+1):(t+freq$Freq[i])]=mean((rmbcsi.rtn$return[(t+1):(t+freq$Freq[i])])^2)
}
n=round(mean(freq$Freq),0)
RT_rolling[1]=rmbcsi.rtn$return[1]
for(i in 2:length(date)){
    if(i<=(n+1)){
        RT_rolling[i]=mean((rmbcsi.rtn$return[1:(i-1)])^2)
    }
    else{
        RT_rolling[i]=mean((rmbcsi.rtn$return[(i-n):(i-1)])^2)
    } 
    }
data_rmbcsi_rtn=data.frame(cbind(cbind(cbind(rmbcsi.rtn,RT),cepu),year_month))
#data_rmbcsi_rtn[,c(1,5)]=apply(data_rmbcsi_rtn[,c(1,5)],2,function(x) as.Date(x,format='%Y-%m-%d'))
data_rmbcsi_rtn$year_month=as.Date(data_rmbcsi_rtn$year_month,format='%Y-%m-%d')
data_rmbcsi_rtn$date=as.Date(data_rmbcsi_rtn$date,format='%Y-%m-%d')
#data_rmbcsi_rtn$return=data_rmbcsi_rtn$return*100
data_rmbcsi_rtn$dcepu=dcepu
data_rmbcsi_rtn$RT_rolling=RT_rolling
head(data_rmbcsi_rtn)

 adfTest(log(huang_cepu$CNEPU),lag=16,type='ct')#中国epu取对数也不平稳
 adfTest(diff(huang_cepu$CNEPU),lag=16,type='ct')#中国EPU取差分平稳
 adfTest(log(aepu_three),lag=16,type='c')   #拒绝原假设，含有截距项的美国lnaepu不平稳
 adfTest(diff(aepu_three),lag=16,type='c')#美国EPU取差分平稳

dccmidas_data=read.xlsx('./thesis/data/dccmidas_data.xlsx',1)
head(dccmidas_data)
str(dccmidas_data)


var_mean=(cmodel_k24_k18$broom.mgarch$estimate[5])+(cmodel_k24_k18$broom.mgarch$estimate[6])*mean(data_rmbcsi_rtn$RT)+(cmodel_k24_k18$broom.mgarch$estimate[8])*mean(data_rmbcsi_rtn$aepu)
resid=cmodel_k24_k18$df.fitted$residuals
resid[is.na(cmodel_k24_k18$df.fitted$residuals)]=(data_rmbcsi_rtn$return-cmodel_k24_k18$broom.mgarch$estimate[1])[is.na(cmodel_k24_k18$df.fitted$residuals)]/var_mean
#write.xlsx(resid,'./thesis/results/resid2.xlsx')

var_mean_usa=(model_k1_36_K2_18$broom.mgarch$estimate[5])+(model_k1_36_K2_18$broom.mgarch$estimate[6])*mean(data_rmbcsi_rtn$RT_usa)+(model_k1_36_K2_18$broom.mgarch$estimate[8])*mean(data_rmbcsi_rtn$aepu)
resid_usa=model_k1_36_K2_18$df.fitted$residuals
resid_usa[is.na(model_k1_36_K2_18$df.fitted$residuals)]=(data_rmbcsi_rtn$return_usa-model_k1_36_K2_18$broom.mgarch$estimate[1])[is.na(model_k1_36_K2_18$df.fitted$residuals)]/var_mean_usa
#write.xlsx(resid_usa,'./thesis/results/resid_usa2.xlsx')

cmodel_k1_12_k2_24=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x =
"cepu",low.freq="year_month", K = 12,x.two = "aepu", K.two = 24, low.freq.two
="year_month",weighting.two = "beta.restricted")#两个都显著，但别的不好
cmodel_k1_24_k2_24=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x =
"cepu",low.freq="year_month", K = 24,x.two = "aepu", K.two = 24, low.freq.two
="year_month",weighting.two = "beta.restricted")#两个都显著，但别的不好
cmodel_k1_12_k2_12=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x =
"cepu",low.freq="year_month", K = 12,x.two = "aepu", K.two = 12, low.freq.two
="year_month",weighting.two = "beta.restricted")#aepu显著
cmodel_k1_24_k2_12=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x =
"cepu",low.freq="year_month", K = 24,x.two = "aepu", K.two = 12, low.freq.two
="year_month",weighting.two = "beta.restricted")#aepu显著

par(mfrow=c(2,2), mar=c(2.1, 2.1, 1.1, 1.1))
plot(cmodel_k24_k24$est.weighting)
plot(cmodel_k24_k24$est.weighting.two)

amodel_k36_k12=fit_mfgarch(data = dccmidas_data, y = "return_usa", x =
"RT_usa",low.freq="year_month", K = 36,x.two = "cepu", K.two = 12, low.freq.two
="year_month",weighting.two = "beta.restricted")#不显著
amodel_k36_k6=fit_mfgarch(data = dccmidas_data, y = "return_usa", x =
"RT_usa",low.freq="year_month", K = 36,x.two = "cepu", K.two = 6, low.freq.two
="year_month",weighting.two = "beta.restricted")#不显著
amodel_k36_k1=fit_mfgarch(data = dccmidas_data, y = "return_usa", x =
"RT_usa",low.freq="year_month", K = 36,x.two = "cepu", K.two = 1, low.freq.two
="year_month",weighting.two = "beta.restricted")#不显著
amodel_k36_k36=fit_mfgarch(data = dccmidas_data, y = "return_usa", x =
"RT_usa",low.freq="year_month", K = 36,x.two = "cepu", K.two = 36, low.freq.two
="year_month",weighting.two = "beta.restricted")#不显著
amodel_k36_k18=fit_mfgarch(data = dccmidas_data, y = "return_usa", x =
"RT_usa",low.freq="year_month", K = 36,x.two = "cepu", K.two = 18, low.freq.two
="year_month",weighting.two = "beta.restricted")#不显著
amodel_k36_k24=fit_mfgarch(data = dccmidas_data, y = "return_usa", x =
"RT_usa",low.freq="year_month", K = 36,x.two = "cepu", K.two = 24, low.freq.two
="year_month",weighting.two = "beta.restricted")#不显著

longcorr=read.xlsx('./thesis/data/longcorr.xlsx',1)
str(longcorr)
longcorr=longcorr[seq(1,12859,4),]
plot(longcorr,type='l')
cor.test(resid,resid_usa,method='pearson')

#自相关处理后的gapc-midas-midas

data=read.xlsx("./thesis/data/dccmidas_data.xlsx",1,encoding= "UTF-8")
data_gspc_rtn=read.xlsx('./thesis/results/arma_data_gspc_rtn_new.xlsx',1,encoding='UTF_8')
armamodel1=auto.arima(data[,2],ic = 'aic')      
armamodel2=auto.arima(data[,3],ic = 'aic')
data_gspc_rtn$new_return=armamodel2$residuals

new_model_k1_36_K2_18=fit_mfgarch(data = data_gspc_rtn, y = "new_return", x = "RT_usa_22",low.freq
="year_month", K = 36,x.two = "aepu", K.two = 18, low.freq.two =
"year_month",weighting.two = "beta.restricted")

new_model_aepu_k18=fit_mfgarch(data = data_gspc_rtn, y = "new_return", x = "aepu",low.freq
="year_month", K = 18,weighting.two = "beta.restricted")#m与w2不显著

new_model_rt_k36=fit_mfgarch(data = data_gspc_rtn, y = "new_return", x = "RT_usa_22",low.freq="year_month", K = 36,weighting.two = "beta.restricted")#m与w2不显著

new_amodel_k36_k12=fit_mfgarch(data = data_gspc_rtn, y = "new_return", x =
"RT_usa_22",low.freq="year_month", K = 36,x.two = "cepu", K.two = 12, low.freq.two
="year_month",weighting.two = "beta.restricted")#不显著

new_amodel_k1_18_k2_12=fit_mfgarch(data = data_gspc_rtn, y = "new_return", x =
"cepu",low.freq="year_month", K = 12,x.two = "aepu", K.two = 18, low.freq.two
="year_month",weighting.two = "beta.restricted")#aepu不显著

table_result_gspc=data.frame(matrix(c(
#output_table(model_rt_k24),
output_table(new_model_rt_k36),
output_table(new_model_aepu_k18),
#output_table(model_aepu_k24),
#output_table(new_model_gspc_cepu),
#output_table(model_k1_24_K2_24),
#output_table(model_k1_24_K2_18),
#output_table(model_k1_36_K2_24),
output_table(new_model_k1_36_K2_18),
output_table(new_amodel_k36_k12),
output_table(new_amodel_k1_18_k2_12)
),nrow=22,byrow=FALSE))
rowname=c('K','K_two','mu','mu_std','alpha','alpha_std','beta','beta_std','gamma','gamma_std','m','m_std','theta','theta_std','w2','w2_std','theta.two','theta_two_std','w2.two','w2_two_std','llh','bic')
rownames(table_result_gspc)=rowname
colnames(table_result_gspc)=paste(rep('model',5),as.character(seq(1,5)),sep='')
write.csv(table_result_gspc,'./thesis/results/arma_gspc_table_result_1230.csv')

#2021-03-01

data_rmbcsi_rtn=read.xlsx('./thesis/results/data_rmbcsi_rtn_new.xlsx',1,encoding='UTF_8')

library(mfGARCH)
cmodel_aepu_k24=fit_mfgarch(data = data_rmbcsi_rtn, y = "return", x =
"aepu",low.freq="year_month", K = 24,weighting.two = "beta.restricted")#aepu在24时显著

data_gspc_rtn_new=data_gspc_rtn
data_gspc_rtn_new$return=data_gspc_rtn$new_return
colnames(data_gspc_rtn)
new_model_aepu_k24=fit_mfgarch(data = data_gspc_rtn_new, y = "return", x = "aepu",low.freq
="year_month", K = 24,weighting.two = "beta.restricted")#m与w2不显著

head(data_gspc_rtn_new)
cresid=cmodel_aepu_k24$df.fitted$residuals
aresid=new_model_aepu_k24$df.fitted$residuals
resid=data.frame(cbind(cresid,aresid))
head(cresid)
for(i in 1:10) print(ArchTest(cresid,lag=i))
#自相关性检验


# 构建得出完整标准残差的函数,mu是model$broom.mgarch$estimate[5],theta是cmodel_aepu_k24$broom.mgarch$estimate[6]
sresid=function(model,mu,theta,alpha,beta,m,gamma){
    # 计算长期波动均值
    var_long_mean=exp(m+theta*mean(model$df.fitted$aepu))
    # 用均值代替控制
    tau=model$df.fitted$tau
    tau[is.na(model$df.fitted$tau)]=var_long_mean
    # 计算短期波动g
    n=length(model$df.fitted$return)
    gamma_r=vector(length=n)
    for(i in 1:n){
        if ((model$df.fitted$return[i]-model$broom.mgarch$estimate[1])<0){
          gamma_r[i]=1
        }else{
          gamma_r[i]=0
        }
    }
    g=vector(length=n)
    constant=1-alpha-beta-gamma/2
    g[1]=1
    for(i in 2:n){
        g[i]=constant+(alpha+gamma*gamma_r[i-1])*((model$df.fitted$return[i-1]-mu)^2)/tau[i]+beta*g[i-1]
    }

    # 计算标准残差序列
    resid=model$df.fitted$residuals
    resid[is.na(model$df.fitted$residuals)]=(model$df.fitted$return-mu)[is.na(model$df.fitted$residuals)]/(sqrt(tau*g)[is.na(model$df.fitted$residuals)])
    data=data.frame(cbind(model$df.fitted$return,cbind(cbind(cbind(g,tau),resid),model$df.fitted$g)))
}

# 自己模拟的g和计算的差不多
mu=cmodel_aepu_k24$broom.mgarch$estimate[1]
alpha=cmodel_aepu_k24$broom.mgarch$estimate[2]
beta=cmodel_aepu_k24$broom.mgarch$estimate[3]
gamma=cmodel_aepu_k24$broom.mgarch$estimate[4]
m=cmodel_aepu_k24$broom.mgarch$estimate[5]
theta=cmodel_aepu_k24$broom.mgarch$estimate[6]

cdata=sresid(cmodel_aepu_k24,mu,theta,alpha,beta,m,gamma)
par(mfrow=c(1,2))
plot(cdata$g[471:3293],type='l',col='red')
plot(cmodel_aepu_k24$df.fitted$g[471:3293],col='black',type='l')
#write.xlsx(resid,'./thesis/results/resid2.xlsx')

mu=new_model_aepu_k24$broom.mgarch$estimate[1]
alpha=new_model_aepu_k24$broom.mgarch$estimate[2]
beta=new_model_aepu_k24$broom.mgarch$estimate[3]
gamma=new_model_aepu_k24$broom.mgarch$estimate[4]
m=new_model_aepu_k24$broom.mgarch$estimate[5]
theta=new_model_aepu_k24$broom.mgarch$estimate[6]
adata=sresid(new_model_aepu_k24,mu,theta,alpha,beta,m,gamma)
#write.xlsx(resid_usa,'./thesis/results/resid_usa2.xlsx')

par(mfrow=c(1,2))
plot(adata$g[471:3293],type='l',col='red',ylim=c(0,70))
plot(new_model_aepu_k24$df.fitted$g[471:3293],col='black',type='l',ylim=c(0,70))

resid=data.frame(cbind(cdata$resid,adata$resid))
colnames(resid)=c('cresid','aresid')
resid$aepu=data_gspc_rtn$aepu
#write.xlsx(resid,'./thesis/data/resid.xlsx')
par(mfrow=c(1,2))
plot(adata$tau[471:3293],type='l',col='red')
plot(new_model_aepu_k18$df.fitted$tau[471:3293],col='black',type='l')

plot(new_model_aepu_k24$df.fitted$residuals[471:3293],col='black',type='l')
plot(adata$resid,col='black',type='l')
length(new_model_aepu_k24$df.fitted$residuals)
head(data_gspc_rtn_new$return)
head(data_gspc_rtn$new_return)
head(data_gspc_rtn$aepu)
head(data_rmbcsi_rtn$aepu)

new_model_aepu_k18=fit_mfgarch(data = data_gspc_rtn_new, y = "return", x = "aepu",low.freq
="year_month", K = 18,weighting.two = "beta.restricted")#m与w2不显著
mu=new_model_aepu_k18$broom.mgarch$estimate[1]
alpha=new_model_aepu_k18$broom.mgarch$estimate[2]
beta=new_model_aepu_k18$broom.mgarch$estimate[3]
gamma=new_model_aepu_k18$broom.mgarch$estimate[4]
m=new_model_aepu_k18$broom.mgarch$estimate[5]
theta=new_model_aepu_k18$broom.mgarch$estimate[6]
adata=sresid(new_model_aepu_k18,mu,theta,alpha,beta,m,gamma)
#write.xlsx(resid_usa,'./thesis/results/resid_usa2.xlsx')

par(mfrow=c(1,2))
plot(adata$g[471:3293],type='l',col='red',ylim=c(0,70))
plot(new_model_aepu_k18$df.fitted$g[471:3293],col='black',type='l',ylim=c(0,70))

resid=read.xlsx('./thesis/data/resid.xlsx',1)
var(as.numeric(resid[,2]))
head(resid)

# 经济政策不确定性对中美股市长期相关性影响代码
# longcorr=read.xlsx('./thesis/results/LongRunCorrvec.xlsx',1)
longcorr=read.xlsx('./results/LongRunCorrvec.xlsx',1)
longcorr0=c(longcorr[720:3293,1][seq(1,2572,20)],tail(longcorr[,1],1))
cepu0=unique(data$cepu[700:3293])[1:130]
aepu0=unique(data$aepu[700:3293])[1:130]
data_reg=data.frame(cbind(cbind(longcorr0,cepu0),aepu0))
data_reg=write.csv(data_reg,'./thesis/results/data_reg.csv')
# 权重函数
midasBetaWeights = function(nlag,param){
    s= seq(1,nlag,1)
    weight = vector(length = nlag)
    for(i in 1:nlag){
        weight[i] = (1-s[i]/nlag)^(param-1)
    }
    weights=sum(weight)
    weight=weight/weights
    weight
}
weight00=midasBetaWeights(36,1.96)
n=130
total=vector(length =200)
for(t in 37:n){
    a=sum(weight00 * cepu0[(t-36):(t-1)])
    total[t]=a
}

total=total[37:130]
a=lm(longcorr0[37:130]~total)

total=vector(length =200)
for(t in 37:n){
    a=sum(weight00 * aepu0[(t-36):(t-1)])
    total[t]=a
}
total_aepu=total[37:130]
b=lm(longcorr0[37:130]~total_aepu)
summary(a)
summary(b)
plot(ts(weight00),type='l')
(1-2/36)^(0.5-1)