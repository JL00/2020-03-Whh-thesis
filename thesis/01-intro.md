---
title: "导论"
author: "Jin"
date: '2019-04-15'
institute: 中南财经政法大学统计与数学学院
csl: ./style/chinese-gb7714-2015-numeric.csl
css: ./style/markdown.css
bibliography: [./Bibfile.bib]
notice: '@*'
eqnPrefixTemplate: ($$i$$)
link-citations: true
linkReferences: true
chapters: true
tableEqns: false
autoEqnLabels: false
classoption: "aspectratio=1610"
---





\cleardoublepage
\pagestyle{emptypage}
\renewcommand{\chapterlabel}{导论}

# 导论 {-}

### 研究背景和意义

#### 研究背景

当今世界形势错综复杂、充满矛盾，大国博弈持续加剧，国际秩序正经历新旧交替的转型过
渡期。美国国家安全战略视大国竞争为首要关切，执意以"美国优先"原则重塑世界秩序，导
致大国博弈加剧，尤其是中美之间的较量博弈。随着中美实力对比的消长，美国对中国的防
范和打压力度逐步增强，中美博弈艰巨复杂。但同时，美国是中国最大的贸易伙伴国和境外
投资主要来源地，中国是美国的最大进出口贸易国，并且我国对美投资一直呈增长态势直至
特朗普上台执政前。总之，两国在经济、贸易和金融领域的关系既有较量，又有合作，
但随着中美贸易争端不断升级，二者关系越来越复杂。

除此之外，欧盟一体化艰难前行，欧洲政治生态内部发生巨大变化，内部矛盾叠加，英国脱
欧也给欧美带来多重震荡。在美国持续破坏多边国际秩序的情况下，欧美保护主义、排外主
义与自顾情绪高涨，不稳定性上升，对全球经济健康发展造成严峻挑战。在此背景下，中国
不断深化改革，优化经济结构，转换增长动力，为应对不断演变的世界变局和为国内经济发
展提供动力。相应地，我国出台一系列举措，但这增加了经济政策的不确定性。如2007年，
实体经济增长过热，央行连续多次加息，避免积累过大的泡沫。当前全球受疫情影响，世界
经济衰退的风险加剧，我国对此采取多项举措。如设立3000亿元低成本的专项再贷款，降准
释放5500亿元资金支持实体经济，加大金融监管以及一系列降息、降准、降费等维护市场
稳定的系列政策措施。而股市作为筹集资金，优化资源配置的重要平台，诸如降息，加强金
融监管等政策都会对股市带来直接或间接的影响。

我国早期的股票市场基本与国际股市处于“隔绝”状态，2001年，中国加入WTO，开始参与到
世界市场经济和多边贸易化体系中去，中国内地的资本市场开始与国际市场产生联系。2003
年瑞士银行敲下QFII(合格境外机构投资者)第一单，标志国内资本市场正式对外开放。之
后，在2005年，我国开展了中国内地证券市场所独有的股权分置改革行动，2007年正式实施
QDII(合格境内机构投资者), 2014年沪港通出世，2016年深港通面世，2018年A股被纳入
MSCI指数，这些既是我国资本市场的国际化进程，也是我国政府为提升金融市场开放度不断
做出的改革。如今中国己经是全球最重要的新兴资本市场之一，中国股市与世界其他国家股
市的联系也越来越密切。而美国占据全球经济的主导地位，中美两国的股市关联性是全球金
融市场自由化趋势下，金融市场相互关联的重要体现。

一国政府为了促进本国经济更好发展，会采取一系列对内和对外的政策调整。而这在世界经
济全球化和金融市场自由化背景下，影响范围不仅仅局限在本国经济。同时，股市与宏观经
济运行息息相关，政策的调整会造成股票市场一定程度的波动，尤其是经济政策。但经济政策
的调整对一国经济，对股市的影响大小如何，无法定量研究。为此，Baker等在2013年后相
继编制了世界21个国家的经济政策不确定性指数，有越来越多的学者和政策研究机构(例如著
名的FRED数据库)开始使用这一系列指数研究政策不确定性对金融市场等的影响。

#### 研究意义

随着国际经济一体化的发展，各国金融市场和各类金融市场之间的界限逐步消失，各国的股
票市场呈现出很高的关联性。股市的关联性是指不同国家或地区的股票资产价格在某段时间
内一齐变动的现象，变动的方向可以是同向也可以是反向。这种关联也可以理解为股市波动
传播或传染的一种具体表现，它说明不同市场上的股票资产价格有着相同的长期运动趋势以
及在短期内有单向或双向的溢出效应。而不断变换的国际形势，加深了经济发展的不确定
性，各国政府为此做出的政策调整导致经济政策不确定性的上升。美国股市作为全球最发达
的股市，同时与中国有密切的经济联系，分析经济政策的不确定性对中美股票市场间关联性
的影响无论是在理论研究还是现实方面都具有重要意义。

首先是理论意义。由于经济政策不确定性和金融数据属于不同频率，学者们在研究经济政策
不确定性与金融市场的关系时，主要采取两种办法进行处理。一种是将高频数据通过平均的
方法转换为低频数据，但这种方法损失了金融数据中蕴含的大量信息。另外一种方法则不改
变数据频率，以近年来备受国内外学者青睐的混频数据采样模型（MIDAS）为主。其中
DCC-MIDAS模型主要适用于相关性研究。但大多数研究只关注相关性本身，并且关于经济政
策不确定性对中美股市关联性影响的理论研究很少,故本文拟引入经济政策不确定这个低频
外生变量，利用DCC-MIDAS模型研究其对中美股市关联性的影响，不仅拓展了模型应用，而
且丰富了相关内容，具有重要的理论意义。

其次是现实意义。从政策制定者的角度来讲，探究经济政策不确定性对中美股票市场关联性
的影响，可以为其提供参考意义。通过调整经济政策，国家实现对股票等金融市场的调控和
监管，使之结构更加合理，制度更加完善，资源得到合理分配，从而为实现资金有序流动，
促进经济健康发展服务。从广大市场参与者的角度来讲，股票市场关系着广大投资者和企业
的切身利益。投资者通过对经济政策不确定性对股票市场关联性影响的合理预期和科学认
识，合理安排自身投资计划，从而实现自身利益。

### 国内外研究现状述评

#### 关于经济政策不确定性的文献综述

经济政策不确定性衡量了经济政策与公众预期之间的偏离程度，为了准确测度这种不确定
性，Baker(2016)编制了22个国家(地区)的经济政策不确定指数(EPU)[@Baker2016]，之后，
Huang等(2020)构造了中国不同类型的经济政策不确定性指数[@Huang2020]。但Baker的经济
政策不确定性提出较早，故该指数受到国内外众学者的关注，并得到了很多领域的认可和应
用。研究范围主要包括经济政策不确定性对对外贸易、就业等宏观经济领域以及微观层面的
企业行为的影响。

1.关于宏观经济领域的研究

大部分学者认为经济政策不确定性对对外贸易、就业率等宏观经济领域的影响是负向的，但
也存在少数学者持不同观点。如Caggiano(2017)研究美国经济衰退期间和扩张期间经济政策
不确定性意外增长对失业率的影响。研究发现，无论是经济衰退期还是扩张期，经济政策不
确定性均会引起失业率增加[@Caggiano2017]。许志伟和王文甫(2019)运用结构向量自回归
方法，得出中国经济结构的变化会加大经济政策不确定性对宏观经济的不利影响[@许志伟2019]。
谢廷宇(2020)在分析经济政策不确定性对就业率的影响时,发现:经济政策不确定性
对不同国家的就业率均存在显著负效应[@谢廷宇2020]。金春雨和张德园(2020)研究了中国
财政政策、货币政策、贸易政策以及汇率与资本项目政策四种类型的经济政策不确定性对宏
观经济的影响。结果显示:四种经济政策不确定性均会导致产出下降[@金春雨2020]。而孙永强等人(2018)
的研究发现，经济政策不确定性并不一定会引起经济波动[@孙永强2018]。Canh(2020)对
2003-2013年间21个经济体的国内经济政策不确定性和外国直接投资(FDI)进行研究，结果显
示:国内经济政策不确定性的增长会对FDI造成不利影响，但全球经济政策不确定性可能会增
加流入该国的外国直接投资[@Canh2020]。杨旭和刘祎(2020)基于中国EPU探讨经济政策不确
定性对进口贸易的影响。结果表明：总体上，经济政策不确定性的上升抑制了进口总额的增
加，但推动了进口质量的相对提升[@杨旭2020]。

2.企业行为等微观经济领域研究成果

经济政策不确定性对企业行为等微观经济领域的影响研究现已得到一致结论:经济政策不
确定性会对企业融资、投资等企业行为造成不利影响。如宋云星（2020）认为,经济政策不
确定性的加剧对我国民营企业融资效率的提升有显著的抑制作用[@宋云星2020]。张光利
(2018)发现经济政策不确定会显著降低企业的融资约束水平[@张光利2018]。韩亮亮(2019)
研究发现:经济政策不确定性的提高会抑制国家或地区的创新产出[@韩亮亮2019]。韩国高
(2014)，李凤羽和杨墨竹(2015)的研究结果均显示:经济政策不确定性对企业投资有抑制作
用[@韩国高2014;@李凤羽2015]。

综合以上研究可以发现，经济政策不确定性对宏微观经济主要是不利影响。而股票市场作为
反应宏观经济的"晴雨表"，直接研究经济政策不确定性对股票市场等金融市场的影响也有重
要意义。因此，接下来重点关注股票市场和经济政策不确定性对股票市场的影响。

#### 关于中美股票市场波动率及关联性研究的文献综述

1.关于经济政策不确定性对中美股票市场波动率影响的文献综述

在Baker发布经济政策不确定性指数以后，国内外对于经济政策不确定性与股市关系的研究
也多了起来。接下来分别介绍经济政策不确定性对中美两国股市波动率的影响。

关于经济政策不确定性对美国股市波动率的影响，学者们即关注整个股市，也关注对不同行业股票
波动率的影响。如Antonakakis等人(2013)的研究表明政策不确定性与股市已实现波动率之间存在时变相关
性，这种相关性在美国经济衰退期表现更强烈[@Antonakakis2013a]。Liu(2015)研究美国经
济政策不确定性对美国股票市场波动的可预测性时，发现较高的经济政策不确定性会导致市
场波动显著增加[@Liu2015]。Wang(2020)通过改变GARCH-MIDAS模型中短期成分和长期成分
的模型设定，对美国标准普尔指数的波动性进行预测,结果显示GARCH-MIDAS模型中短期波动
性成分的不对称性和极端波动性效应的引入明显提高了预测能力[@Wang2020]。Yu(2018)在
进行美国行业级股票市场波动率研究时，发现美国经济政策不确定性对不同行业股票的波动
率影响有差异，全球经济政策不确定性亦是如此[@Yu2018a;@Yu2018b]。

关于经济政策不确定性对中国股市波动率的影响，国内的研究内容也同样丰富。如陈进国
(2014)以上证综指为代表，通过DCC-MGARCH模型和VARMA-BEKK-MGARCH模型研究中国股票市
场，发现股票市场与政策不确定性之间具有显著的相关性和很强的时变性[@陈国进2014]。
王永莲(2017)利用GARCH-MIDAS模型，研究发现我国经济政策不确定性对股票市场(沪深300
指数)波动的影响相对较弱,经济政策不确定性的上升会在一定程度上会加大我国股票市场的
波动[@王永莲2017]。雷立坤(2018)在运用GARCH-MIDAS模型分析经济政策不确定性(EPU)对
上证综指波动率的影响时，得出：EPU指数能够很好地解释我国股市波动的长期成分,并显著
改善对上证综指波动率的预测精度[@雷立坤2018]。夏婷和闻岳春(2019)同样使用
GARCH-MIDAS模型进行研究，但结果表明:中国经济政策不确定性指数对A股的波动率无显著
影响，但会影响B股的长期波动率[@夏婷2018]。石强等(2019)利用GARCH-MIDAS模型得出，
经济政策不确定性对股市波动(上证综合指数)的影响呈现出阶段差异性[@石强2019a]。

2.关于中美股票市场间关联性研究的文献综述

自中国金融市场改革以来，中国股票市场与世界股票市场的联系日益紧密，尤其是发达国家。
目前，已有很多文献关注中美股市的关联性，并研究其背后的影响因素。故接下来对这部分
文献作简要回顾。

第一、中美股票市场间关联性研究

随着中国对外开放的不断推进，中美股市关联性呈现出逐渐增强的特征，并且广大学者们关
注QDII制度、金融危机和贸易战等重大事件对中美股市关联性造成的阶段性影响。如较早时
期，韩非和肖辉(2005)以2000年1月1日到2004年12月31日间的美国标准普尔指数和上证A指
为对象，研究中美股市的相关性，结果发现：中美股市的相关性很弱[@韩非2005]。之后，
胡秋灵和刘伟(2009)基于2007年8月1日到2008年12月31日的日数据,利用VAR模型研究中美股
市的联动性，得出：二者之间具有一定的联动性[@胡秋灵2009]。张兵，范致镇和李心丹
(2010)以2001年12月12日到2009年1月23日上证指数与道琼斯指数的日交易数据为样本,分阶
段检验了中美股市的联动特征。主要结论是：中美股市之间不存在长期均衡关系；但在QDII
实施之后,美国股市对中国股市的波动溢出呈现不断增强之势[@张兵2010]。李红权，洪永淼和汪寿阳(2011)利用2005年7月26日至2009年
7月7日间的中美股市数据，研究发现：在次贷危机后美股与A股的相关关系显著提升[@李红权2011]。
国外学者Kenourgios,Samitas和Paltalidis(2011);Aloui,Aïssa和Nguyen(2011);Singh和
Kaur也证实了金融危机期间，中美股市的关联性[@Kenourgios2011;@Aloui2011;@Singh2014]。
饶建萍，王波和唐铭惠(2019)利用2017年1月3日到2019年6月7日间的数据，通过ECM模型研
究贸易战前后中美两国股市的联动性。研究发现:贸易战前后,我国股票市场都受到美国股票
市场的影响,而反过来,中国股票市场的走向几乎不会对美国股市造成影响;贸易战之后，两
国股市的联动性降低[@饶建萍2019]。

第二、中美股票市场间关联性影响因素研究

除了关注中美股市相关性本身的特征之外，也有学者探究造成这种关联性逐渐增强的原因。
如游家兴和郑挺国(2009)运用DCC-GARCH模型研究从1991年到2008年中国与美国等股票市场
联动的时变轨迹及其特征,研究发现中美股票市场联动性虽有增强的趋势,但处于较低的水
平，并将中美股市关联性的增强归因于中国股市的不断开放[@游家兴2009]。杨雪莱和张宏
志(2012)通过在(DCC)GARCH模型中引入宏观经济变量，得出,美国货币政策冲击的结构性突变是
中美股市联动的最重要原因[@杨雪莱2012]。刘阳和高惠(2012)采用DCC-GARCH方法量化中美股
票市场的长期联动关系。研究发现，中美两国间双边贸易依存度、金融开放度、经济周期差
异度、汇率形成机制以及外部冲击都对两国股市联动关系产生影响[@刘阳2012]。张金萍和
王准(2014)以1998年1月1日至2011年12月30日中国上证指数收益率和美国道琼斯指数收益率
为数据样本,建立DCC-GARCH模型。研究发现，中美股市相关性经历了由负为正的过程。并且
中国加入WTO、引入QFII和QDII、中国股权分置改革、全球金融危机和美债危机都对中美股
市联动性产生了一定影响[@张金萍2014]。龚金国和史代敏(2015)以2005年7月26日至2009年7
月7日间的上证综指和美国标准普尔指数为研究对象研究中美股市联动。得出，中国金融自
由化不是中美股市联动的原因，反而，贸易强度的变化是联动增强的主要原因[@龚金国2015]。
Chiang和Chen(2016)的研究发现，中美股票市场之间的收益率相关性是随时间变化
的，显示出的结构性变化是由中国采用金融自由化和发生全球性金融危机而引发的[@Chiang2016]。

#### 混频数据模型相关文献综述

从之前的文献综述部分可以看出，对于股市波动率的研究方法，已有学者使用GARCH-MIDAS
模型。而对于股市相关性的研究方法，虽有DCC-GARCH、VAR等模型可供选择，但在具体计算
时，必须保证所使用数据是同频的。本文拟采用基于混频数据的GARCH-MIDAS模型和
DCC-MIDAS模型探究经济政策不确定性对股市波动率及关联性的影响，所用数据分别是月度
数据和日度数据，无需进行同频处理。故接下来着重介绍有关这两种模型的国内外文献。

1.GARCH-MIDAS模型的提出及应用

GARCH-MIDAS模型于2013年首次被提出。Engle等
(2013)受到混频数据采样（MIDAS）回归模型可以以更高采样频率的时间序列数据作为自变
量的启发，提出广义自回归条件异方差混频数据抽样模型(GARCH-MIDAS),研究股市波动与宏
观经济活动之间的关系[@Ghysels2002;@Engle2013]。他们使用MIDAS方法将宏观经济变量与
长期波动率联系起来，同时使用GARCH过程描述每日股票市场的短期波动率，所以叫做
GARCH-MIDAS模型。该模型显示出良好的预测能力。由于通常的混频数据都要预先处理，先
转换为同频数据再进行下一步研究，GARCH-MIDAS模型，解决了在模型中同时使用不同频率
数据的难题，故在宏观经济学和金融学中具有广泛的适用性。

国内外学者对于GARCH-MIDAS模型的应用除了上一节介绍的股票市场，还有黄金市场、原油市场、债券
市场等领域。如Asgharian(2013)利用GARCH-MIDAS模型研究意外通货膨胀，失业率等宏观经
济变量对美国股市波动率的影响[@Asgharian2013]。Wei(2017)利用GARCH-MIDAS模型探究经
济政策不确定性指数、石油需求量，供应量等众多影响石油价格的因素中，最重要的因子[@Wei2017]。
Fang(2018)利用引入全球经济政策不确定性指数的GARCH-MIDAS模型，研究黄金市场的波动
率并做出预测，GARCH-MIDAS模型的预测性能明显优于GARCH(1,1)模型[@Fang2018]。李佳
(2019)应用GARCH-MIDAS模型研究中国宏观经济景气指数对沪铜期货长短期波动的影响，张
屹山(2018)利用GARCH-MIDAS模型研究宏观经济波动对银行间债券市场和利率互换市场波动
的影响[@李佳2019;@张屹山2018]。Zhou等(2019)利用引入中美EPU比率的
GARCH-MIDAS模型来研究中美经济政策不确定性对汇率波动的影响,并得出:GARCH-MIDAS模型
的样本外波动率预测性能比优于传统GARCH类模型[@Zhou2019]。

2.DCC-MIDAS模型的提出和应用

Colacito,Enger和Ghysels受GARCH-MIDAS模型的启发，
将Engle(2002)提出的动态条件相关性模型(Dynamic Conditional Correlation Model，下
面简称DCC模型)与GARCH-MIDAS模型结合起来，于2011年首次提出混频数据抽样动态相关系
数模型(DCC-MIDAS)[@Engle2002;@Colacito2011]。他们将动态相关系数也分为长期相关成
分和短期相关成分。短期相关成分可以由DCC结构中类似GARCH(1,1)的动态过程描述，而资
产之间的长期动态相关性由MIDAS模型对滞后的已实现相关系数加权获得。但Colacito等人
的长期相关成分并未与宏观经济变量相关联，不能像GARCH-MIDAS的长期波动一样，直接将
宏观经济变量作为长期动态相关系数的解释变量。基于此，Conrad,Loch和Rittler(2014)提
出拓展的DCC-MIDAS模型，研究宏观经济变量对美国石油和股票市场相关性的影响[@Conrad2014]。

之后，DCC-MIDAS模型在金融领域有了更加广泛的应用。如Asgharian(2016)使用同时含有已
实现相关系数和宏观金融变量的DCC-MIDAS研究通货膨胀、利率等因素对美国股债两市相关
性的影响，结果表明，在经济疲软时期，二者的相关性很小且为负[@Asgharian2016]。
Fang,Yu,and Li（2017)利用修改的DCC-MIDAS模型,综合考虑结构性变化，引入虚拟变量研
究经济政策不确定性对美国股债两市相关性的影响，结果发现经济政策不确定性对二者的相
关性具有显著的负向作用[@Fang2017]。同时，Fang等人(2018)在对原油和美国股市相关性
的研究中，发现经济政策不确定性对原油股票的相关性具有显著的积极影响，并且这种关系
在金融危机期间发生了结构性变化[@Fang2018a]。Yang（2018）利用DCC-MIDAS模型研究通
货膨胀率和期限利差等经济因素对石油价格和汇率的相关性[@Yang2018]。

由于外国学者最先提出DCC-MIDAS模型，并最先对其做了拓展，故国外学者在这方面的研究
要略早于国内学者。下面介绍国内对DCC-MIDAS模型的应用情况。

姚尧之和刘志峰(2017)采用
DCC-MIDAS模型研究沪深港股市之间的动态相关性。研究结果表明，总体上，内地股市和中
国香港股市之间的长短期相关性呈现出逐步增强的趋势[@姚尧之2017]。
张屹山(2018)利用DCC-MIDAS模型研究银行间债券市场和利率市场的相关性。得出这两者之
间呈负动态条件相关性且有逐渐增强的趋势[@张屹山2018]。
孙毅和秦梦(2019)利用DCC-MIDAS模型研究中美大豆期货市场的相关性，发
现二者之间具有短期和长期动态相关性[@孙毅2019]。刘振华(2019)利用DCC-MIDAS模型研究
原油与股票市场的相关性，结果显示：经济衰退期的相关性比其他时期更高且震动更强烈[@刘振华2019]。
以上DCC-MIDAS模型均以已实现条件相关系数为外生变量，之后有学者开始研究宏观经济变
量对动态相关系数的影响。李佳和茆训诚(2019)以沪铜期货和上证A股为研究对象，利用含
有中国宏观经济景气指数的DCC-MIDAS模型考察期货市场和股票市场在金融危机前后的结构
性变动。实证结果表明:在宏观外部环境和经济景气下滑时期, 两者之间的相关性显著上升,
而在宏观经济平稳时期, 相关性则转而缓慢下降[@李佳2019]。张宗新(2020)以股债两市为
研究对象，通过对DCC-MIDAS模型进行拓展，研究经济政策不确定性对金融市场间流动性协
同运动的影响。结果发现：经济政策不确定性对股债两市流动性的正相关性呈负效应，但在
金融周期的拐点处，经济政策不确定性会提高二者的正相关性[@张宗新2020]。


#### 文献评述

综合以上所述文献综述可知，经济政策不确定性对宏微观经济主要是不利影响。关于经济政
策不确实性对股票市场波动率的影响，各研究结论还存在差异，在具体研究时，大多数使用
GARCH-MIDAS模型，该模型能够提高经济政策不确定性对股市波动率的预测精度。故本文将
继续使用GARCH-MIDAS模型，重点关注经济政策不确定性对股市波动率的影响以及呈现出的
阶段性特征。

在中美股市的关联性方面，国内外学者已有一致结论，认为两个市场之间存在相关性，且表
现出阶段性特征。金融危机、美国货币政策冲击的结构性突变以及中国自身在金融市场的逐步改革和
开放是促使两个市场关联性逐步增强的原因，同时，受政策影响，诸如股权分置改
革、贸易战等重大事件也会对中美两国的股票市场相关性造成影响。但目前尚未有文献研究
经济政策不确定性对中美股市相关性的影响。

在方法的选择上，DCC-GARCH，VAR等模型是相关性研究中较为常见的。但无法加入外生变量
以直接研究经济政策不确定性对两个金融市场相关性的影响，也无法处理同时引进不同频率
数据的问题，因此，本文拟采用拓展的DCC-MIDAS模型，研究经济政策不确定性对中美股市
相关性的影响。并借鉴已有文献，以金融危机为分界线，分别研究金融危机前后这种影响的
大小及差异。


\pagestyle{mpage}

<!-- # 参考文献 -->
[//]: # (\bibliography{Bibfile})

