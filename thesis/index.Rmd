--- 
title: "中美股市长短期波动率及动态相关性研究"
author: ""
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
css: ./style/markdown.css
autoEqnLabels: true 
eqnPrefixTemplate: ($$i$$)
linkReferences: true
csl: ./style/chinese-gb7714-2015-numeric.csl
link-citations: true
lot: true
lof: true
listings: true

---
